<?php
	//Start the session
	
	function errorHandler($n, $m, $f, $l) {
		header('Location: www.localhost.com/error.php');
	}
	
	session_start();
	set_error_handler('errorHandler');
	
	$_SESSION["_ps_h"] = True;
	$_SESSION["debug_id"] = True;
	$_SESSION["tms"] = date("Y/m/d");
	
	
	$more_entropy = true;
	$_SESSION["uid"] =  uniqid();
	
	
	
	

	//string uniqid ([ string $prefix = "" [, bool $more_entropy = false ]] );
	
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" lang="en">
	<head>
		
		<!link rel="stylesheet" href="/assets/js/jquery/jquery-ui-1.12.0/jquery-ui.css">
		<link rel="stylesheet" href="/assets/js/jquery/jquery-ui-1.12.1.cus	tom/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="/s/assets/css/base.css"/>
		
		<!link rel="stylesheet" type="text/css" href="/assets/css/base/news_small.css"/>
		
		<script src ="/assets/js/chart/chart.js" type="text/javascript"></script>
		<link rel="<link href='https://fonts.googleapis.com/css?family=Cinzel:400,900,700' rel='stylesheet' type='text/css
		<!link rel="stylesheet" type="text/css" href="/s/assets/css/theme-dark.css"/>
		
		<script src="/assets/js/jquery/jquery-3.1.0.min.js" type="text/javascript"></script>
		<script src="/assets/js/jquery/jquery-ui-1.12.0/jquery-ui.js" type="text/javascript"></script>
		<script src="/s/assets/js/base.js" type="text/javascript"></script>
		<script src="/assets/json/jsinput.json" type="text/javascript"></script>

				
		<meta charset="utf-8">		
		<meta name="author" content="The Hourly Update">
		<meta name="keywords" content="Front Page, Live News | World News | 24 Hour News">
		
		<meta name="description" content="The Hourly Update | 24 Hour Dynamic News Search Engine">
		<meta name="description" content="The Hourly Update | 24/7 Live News Search Engine, filter world events, live in your browser!">

		<title>The Hourly Update | 24 Hour Dynamic News Search Engine</title>
		<script>
			$(document).ready(
				function() {
				
				/*$( "#nav-progressbar" ).progressbar({
					value: 37
				});*/
					fb_init();
				}
			);
		</script>
	</head>
	<body class="theme-light">
		
		
		
		<div class="page-container">
			<div class="" id ="page-content">
				<noscript>
					This service requires Javascript (JS), if it is disabled please check your browsers settings.
					It is also recommended you upgrade to the latest release of your browser for maximum compatibility with some advanced features on this site.
					Some modern mobile browser releases are supported, but not recommended, if on mobile please use the app or the mobile page.
				</noscript>
				<div class="ui-shadow" id="topbar">
					<div id="topbar-alignment-wrapper">
						<div id="topbar-left"><div id="topbar-date-output">#OUTPUT#</div></div>
						<div id="topbar-center">
						
						</div>
						<div id="topbar-right">
							<a href="about.html" class="topbar-link">Settings</a>
							
						</div>
					</div>
					<div id = "navbar">
						<div id="navbar-time-output">#OUTPUT#</div>
						<div id="navbar-options">
							
							<div class="navbar-button">
								<label class="switch">
									<input id="input-sidebar-btn" type="checkbox">
									<div class="slider rround"></div>
								</label>
							</div>
							
						</div>
						<div id = "navbar-alignment-wrapper">
							<div id="btn" class= "navbar-tab">-</div>
							<div id="n24" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n23" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n22" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n21" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n20" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n19" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n18" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n17" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n16" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n15" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n14" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n13" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n12" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n11" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n10" class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n9"  class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n8"  class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n7"  class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n6"  class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n5"  class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n4"  class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n3"  class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n2"  class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="n1"  class= "navbar-tab"><span></span><div class="navbar-alert-tag"></div></div>
							<div id="btn" class= "navbar-tab">+</div>
						</div>
						<div id="nav-progressbar"></div>
					</div>
				</div>
				<div class= "data-container" id="data-wrapper">
					
					<div class="data-content" id="data-grid-wrapper">
						<div class = "data-content data-left" id="data-grid" style="width: 100%;">
							<div class="content-bar-container content-bar-clamp">
								<div class="content-bar-content-wrapper">
									<div class="content-bar-content-container">
										<div class="content-bar-header">Fri Mar 10 2017: 16:00
										
											<label class="switch">
												<input id="input-sidebar-btn" type="checkbox">
												<div class="slider rround"></div>
											</label>
										
										</div>
										<div class="content-bar-content">
											<a href="http://www.bbc.co.uk/sport/gymnastics/39299084" target="_blank"><div class="newsElementContainer-box"><div class="newsElement-host">BBC NEWS</div><div class="newsElement-fname">undefined</div><div class="newsElement-content"><img src="/assets/img/error/404-grey.png"><div class="newsElement-title">USA Gymnastics: Steve Penny resigns as president and chief executive</div><div class="newsElement-text">The president and chief executive of USA Gymnastics resigns in the wake of the federation's handling of sexual abuse allegations concerning a former team doctor.... (Read more at: HOST_LOOKUP_KEY: 0)</div></div><div class="newsElement-footer"><div class="newsElement-time">2017-03-16 20:02:56</div></div></div></a>
										</div>
									</div>
									<div class="content-bar-sider-container">
										<div class="content-bar-sider">
											<img src="http://c.files.bbci.co.uk/FD9D/production/_94952946_3cd20787-bdb9-4d3d-bedb-bfda7ab0c87d.jpg">
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class = "data-content data-left" id="data-grid" style="width: 100%;">
							<div class="content-bar-container content-bar-clamp">
								<div class="content-bar-header">Fri Mar 10 2017: 16:00</div>
								<div class="content-bar-content-wrapper">
								<div class="content-bar-content">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</div>
									<div class="content-bar-sider">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</div>
								</div>
							</div>
						</div>
						<div class = "data-content data-left" id="data-grid" style="width: 100%;">
							<div class="content-bar-container content-bar-clamp">
								<div class="content-bar-header">Fri Mar 10 2017: 16:00</div>
								<div class="content-bar-content-wrapper">
								<div class="content-bar-content">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
								"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?""Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
								</div>
									<div class="content-bar-sider-container">
									<div class="content-bar-sider"><img></img></div>
									</div>
								</div>
							</div>
						
						</div>
					</div>
					<div class = "data-content data-right" id="data-sidebar">
						<div id = "data-sidebar-hidden-title">
							<div class ="title">The<strong>Hourly</strong>Update<div class="title-subscript">Est. 2016</div></div>
						</div>
						<div class="content">
							<div class="hoz-table" id="input_btn_group_sidebar">
								<span>Search</span>
								<span>Graphs and Viz</span>
								<span>Settings</span>
							</div>
							<canvas id="chart_BufferStat1" height="200" width="900"></canvas>
							<div class="data-content -ui-init-hidden" id="data-view-search">
								<h1>Search</h1>
								<input type="text" id="text1">
								<label for="text1">Text</label>
							</div>
							<div class="data-content ui-init-hidden" id="data-view-graphs">
								<h1>Graphs</h1>
							</div>
							<div class="data-content ui-init-hidden" id="data-view-settings">
								<h1>Settings</h1>
								<label class="switch">
									<input id="input-sidebar-btn" type="checkbox">
									<div class="slider round"></div>
								</label>
								<label class="switch">
									<input id="input-sidebar-btn" type="checkbox">
									<div class="slider round"></div>
								</label>
								<label class="switch">
									<input id="input-sidebar-btn" type="checkbox">
									<div class="slider round"></div>
								</label>
								<label class="switch">
									<input id="input-sidebar-btn" type="checkbox">
									<div class="slider round"></div>
								</label>
							</div>
							<div class="data-content ui-init-hidden" id="data-view-context">
								<h1>UNUSED CURRENTLY</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>