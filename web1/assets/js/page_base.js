"use strict"; // Obviously


/*
	NOW, YOU MIGHT BE ASKING WHY THIS HAS A VERY UGLE
	NAMING SCHEME
	
	Well, it does mean something, and I ended up writing it like this, to enforce consistent function naming and object naming in a very loose language
	
	functions are prefixed with "f"
	Eg: fv_foobar()

	Next, the return type, for instance:
		v - void
		b - boolean
		ob - javascript object
		str - string
		i/int - int
		d - double
		dom - Document Object
		
	__ means strictly internal and private to the file
	_ means internal, but can be called by external functions, not really used
	no prefix means it's public
	
	Other than that, it's somewhat consistant, kinda.
	
	In_ is used to tell everyone it's an input argument, I need to change more argument variables to this
	
	
	FULL_CAPITAL_LETTERS
	CONST_OR_LIKE_THIS

	Means it's a definition, constant or flag. Explicit or otherwise

*/


/*
	_a_lookup: table
	Cached string table for rss sites, 
	this will be sent to the client by the 
	server in future, on the first request.
*/
var _a_lookup = [
    {
        a: [
            "BBC",
            "The Guardian",
            "The Telegraph",
            "Reuters",
            "The Daily Mail",
            "The Independent",
            "Sky News",
            "The Times",
            "LA Times"
        ]
    }, {
        a: [
            "CNN",
            "Sports Illustrated",
            "Wall Street Journal"
        ]
    }
];

/*a:{   
        "BBC",
        "Guardian",
        "Reuters",
    },
    b: {
            "CNN", 
            "Sports Illustrated", 
            "Wall Street Journal"
        }
];*/

var _str_lookup_namerica = ['CNN', 'Sports Illustrated', 'Wall Street Journal'];
function fstr_getLookupStr(In_int_area, In_int_gk) {
    return _a_lookup[In_int_area].a[In_int_gk];
}


/*
	fob_sqltojDate
	ARGUMENTS: time string in mySQL format
	RETURNS object

	Strip mySQL timestring into a valid javascript time object
*/
function fob_sqltojDate(sqlDate) {
    //console.log("Date: ", sqlDate);
    var sqlDateArr1 = sqlDate.split("-");
    var sYear = sqlDateArr1[0];
    var sMonth = (Number(sqlDateArr1[1]) - 1).toString();
    var sqlDateArr2 = sqlDateArr1[2].split(" ");
    var sDay = sqlDateArr2[0];
    var sqlDateArr3 = sqlDateArr2[1].split(":");
    var sHour = sqlDateArr3[0];
    var sMinute = sqlDateArr3[1];
    var sqlDateArr4 = sqlDateArr3[2].split(".");
    var sSecond = sqlDateArr4[0];
    var sMillisecond = 0;
    //console.log("Decomposed YEAR: Year [%s], Month [%s], Day [%s]", sYear, sMonth, sDay);
    //console.log("Decomposed TIME: Hour [%s], Minute [%s], Second [%s]", sHour, sMinute, sSecond);
    var obj = new Date(sYear, sMonth, sDay, sHour, sMinute, sSecond, sMillisecond);
    return obj;
}

/*
	fdom_setAttrib
	ARGUMENTS: dom node
				Name of the attrib
				content of the attribute
	RETURNS: dom node, with attribute added
*/
function fdom_setAttrib(node, name, text) {
    var attrib = document.createAttribute(name);
    attrib.value = text;
    node.setAttributeNode(attrib);
}

/*
	fdom_setText
	ARGUMENTS: dom node
				text
	RETURNS: dom node, with text node added
*/
function fdom_setText(node, text) {
    var textnode = document.createTextNode(text);
    node.appendChild(textnode);
}

function _fob_generate_button() {
    var root = document.createElement("lable");
    fdom_setAttrib(root, "class", "switch");

    var input = document.createElement("input");
    fdom_setAttrib(input, "type", "checkbox");
    fdom_setAttrib(input, "value", "false");
    fdom_setAttrib(input, "id", "tab_" + p++);
   
    root.appendChild(input);
    
    var slider = document.createElement("div");
    fdom_setAttrib(slider, "class", "slider rround");
    root.appendChild(slider);

    return { r: root, i: input };
}

/*
	_fob_build_NewsElementDOM
	ARGUMENTS:	Host string
				Group name string
				Feed name string
				RSS Title string
				RSS Description string
				RSS Link string
				Javascritp Date string
	RETURNS: Constructed DOM object
*/
function _fob_build_NewsElementDOM(
    strhost,
    strgroupname,
    strfeedname,
    strthumb,
    strtitle,
    strdescription,
    strlink,
    strpub) {
    var root = document.createElement("a");
    fdom_setAttrib(root, "href", strlink);
    fdom_setAttrib(root, "target", "_blank");

    var box = document.createElement("div");
    fdom_setAttrib(box, "class", "newsElementContainer-box");
    var pull = document.createElement("div");
    fdom_setAttrib(pull, "class", "newsElementContainer-pull");
    fdom_setText(pull, "Pull down");

    var host = document.createElement("div");
    fdom_setAttrib(host, "class", "newsElement-host");
    fdom_setText(host, strhost);
    box.appendChild(host);

    var fname = document.createElement("div");
    fdom_setAttrib(fname, "class", "newsElement-fname");
    fdom_setText(fname, strfeedname);
    box.appendChild(fname);

    var content = document.createElement("div");
    fdom_setAttrib(content, "class", "newsElement-content");

    var box_img = document.createElement("img");
    fdom_setAttrib(box_img, "src", strthumb);
    content.appendChild(box_img);

    var title = document.createElement("div");
    fdom_setAttrib(title, "class", "newsElement-title");
    fdom_setText(title, strtitle);
    content.appendChild(title);

    var text = document.createElement("div");
    fdom_setAttrib(text, "class", "newsElement-text");

    if (strdescription.length > 0) {
        var range = document.createRange();
        var fragment = range.createContextualFragment(strdescription);
        var firstNode = fragment.firstChild;
        text.appendChild(firstNode);
    } else {
        dom_setText(text, strdescription);
    }

    content.appendChild(text);
    box.appendChild(content);

    var footer = document.createElement("div");
    fdom_setAttrib(footer, "class", "newsElement-footer");

    var date = document.createElement("div");
    fdom_setAttrib(date, "class", "newsElement-time");
    fdom_setText(date, strpub);
    footer.appendChild(date);
    box.appendChild(footer);
    root.appendChild(box);
    return root;
}
function _fob_build_elementContainerTableDom(index) {
    var root = document.createElement("div");
    fdom_setAttrib(root, "id", "tbl_ref_" + index);
    fdom_setAttrib(root, "class", "news-group-table");
    return root;
}
function _fob_build_ContainerWrapperDom() {
    var root = document.createElement("div");
    fdom_setAttrib(root, "id", "data-grid");
    fdom_setAttrib(root, "class", "data-content data-left");
    return root;
}
function _fob_build_elementContainerDom(In_Str_groupDate, In_b_clamp) {
    var root = document.createElement("div");
    //fdom_setAttrib(root, "class", "content-bar-container");//+ ((In_b_clamp == true) ? "content-bar-clamp" : ""));
    fdom_setAttrib(root, "class", "content-bar-container -content-bar-clamp content-ui-contrast"); //ROOT
    var align_wrapper = document.createElement("div");
    fdom_setAttrib(align_wrapper, "class", "content-bar-content-wrapper"); //padding wrapper
        /*Main content*/
        var container = document.createElement("div");
        fdom_setAttrib(container, "class", "content-bar-content-container"); //main element content container
    //tfw, my website design is blazing speedy
        var header = document.createElement("div");
        fdom_setAttrib(header, "class", "content-bar-header"); //Header bar
        //fdom_setText(header, In_Str_groupDate);

        var header_align = document.createElement("div");
        fdom_setAttrib(header_align, "class", "content-bar-header-align");
        //fdom_setText(header_align, In_Str_groupDate);
        
        var hleft = document.createElement("div");
        fdom_setAttrib(hleft, "class", "content-bar-header-left");
        fdom_setText(hleft, In_Str_groupDate);
        header_align.appendChild(hleft);

        var hright = document.createElement("div");
        fdom_setAttrib(hright, "class", "content-bar-header-right");
        header_align.appendChild(hright);
        header.appendChild(header_align);
        header.appendChild(header_align);
        
    /* var t = document.createElement("input");
    fdom_setAttrib(t, "type", "checkbox");
    root.appendChild(t);*/
       // header.appendChild(_fob_generate_button().r);
        //container.appendChild(header);
        align_wrapper.appendChild(header);

        var contentlistwrapper = document.createElement("div"); //content-wrapper
        fdom_setAttrib(contentlistwrapper, "class", "content-bar-content");
        container.appendChild(contentlistwrapper);
        //fdom_setText(contentlistwrapper, "CONTENT");
        
       // var elemcontainer = document.createElement("div"); //wrapper
        //fdom_setText(elemcontainer, "Wrapper");
       // contentlistwrapper.appendChild(elemcontainer);   
    align_wrapper.appendChild(container);

        /*Toggled side content*/
        var side_content_wrapper = document.createElement("div");
        fdom_setAttrib(side_content_wrapper, "class", "content-bar-sider-container");
        var side_content = document.createElement("div");
        fdom_setAttrib(side_content, "class", "content-bar-sider");
        side_content_wrapper.appendChild(side_content);
        align_wrapper.appendChild(
            side_content_wrapper
            //container
            );

    //align_wrapper.appendChild()
    root.appendChild(align_wrapper);
    let ret = {
        root: root,
        mwrapper:
            container,
            //contentlistwrapper,
        mcontent:
            contentlistwrapper,
            //elemcontainer,
        scontent: side_content,
        swrapper: side_content_wrapper,
    };
    return ret;
}

function fint_percentof(intValue1, intValue2) {
    return (intValue1 / intValue2) * 100;
}
function fint_percent(intPercentage, intValue) {
    return (intPercentage / 100) * intValue;
}
/*	fstr_strPadInt
	
	ARGUMENTS: integer
	RETURNS: string
	Returns a formatted string from an integer
	
	If integer is less than 10, pad it with a leading zero, else just return the string with no modification.
*/
function fstr_strPadInt(intT) {
	return (intT < 10) ? "0" + intT.toString() : intT.toString();
	/*if (intT < 10) {
        return "0" + intT.toString();
    }
    else {
        return intT;
    }*/
}


/*See if element is inside the viewport*/
function fb_boundinginviewport(el) {
    //special bonus for those using jQuery
    /*if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }*/
    var rect = el.getBoundingClientRect();
    return (
        rect.top >= 20 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}

/* 
	fstr_timePadded
		ARGUMENTS:
			Javascript Date object
		RETURNS:
		Formatted string.
		EX: 18:19:20
	*/
function fstr_timePadded(In_ob_time) {
    return fstr_strPadInt(In_ob_time.getHours()) + ":"
        + fstr_strPadInt(In_ob_time.getMinutes()) + ":"
        + fstr_strPadInt(In_ob_time.getSeconds());
}

/*
	fstr_timePadded
	ARGUMENTS: 
		Javascript date object;
		
	RETURNS:
	Takes the hour in date object and formatted with trailing zeros.
	
	EX: 18:00
*/
function fstr_timeDateShort(In_ob_time) {
    return In_ob_time.toDateString() + ": " + In_ob_time.getHours() + ":00";
}

// Not used
function fstr_timeDate(In_ob_time) {
    //return In_ob_time.getfstr_timePadded(In_ob_time);
	return null;
}
function fint_avail_width(){
	return window.screen.availWidth;
}
function fint_avail_height(){
	return window.screen.availHeight;
}
function fint_screen_width(){
	return window.screen.width;
}
function fint_screen_height(){
	return window.screen.height;
}
function fint_window_height(){
	return $(window).height();
}
function fint_window_width(){
	return $(window).width();
}

function fob_generate_DispatchObject() {
    return {
        __b_runLock: false,
    };
}
function fob_generate_UIGate() {
    let active = false;
    return this;
}

/* DOM HANDLERS*/
var __wdom_output_topbar_dstring = ("#topbar-date-output");
var __wdom_dataContainer_DomGrid = ("#data-grid-wrapper");
var __dom_data_sidebar_wrapper = ("#data-sidebar");
var __dom_data_main_wrapper = ("#data-grid-wrapper");
var __dom_data_sidebar = ("#data-sidebar");
var __dom_navbar_container = ("#navbar");
var __a_dom_data_sidebar = new Array();

__a_dom_data_sidebar.push(("#data-view-search"));
__a_dom_data_sidebar.push(("#data-view-graphs"));
__a_dom_data_sidebar.push(("#data-view-settings"));

//var __dom_data_sidebar_Settings = ("#data-view-settings");
//var __dom_data_sidebar_Graphs = ("#data-view-graphs");

function __fv_ui_ScrollViewPortTo(__int_ViewportOffset) {
    $('html, body').animate({
         scrollTop: __int_ViewportOffset - 50
        }, PAGE_ANIMATION * 2, "swing"
	);
}
var _ob_chart_options = {
    responsive: true,
    maintainAspectRatio: true,
    scales: {
        yAxes: [{
            gridLines: {
                display: false
            },
            ticks: {
                beginAtZero: true
            }
        }],
        xAxes: [{
            ticks: {
                beginAtZero: true
            },
            gridLines: {
                display: false
            }
        }]
    }
};
var _ob_chart_database= {
    labels: ["Time"],
    datasets: [{
        label: '',
        data: [0],
        /*backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
        ],
        borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ],*/
        borderWidth: 1
    }]
};

var __dom_chart_BufferStat1;
var __obj_chart_BufferStat1;
function _fb_init_chartBase() {
    console.log("[_fb_init_chartBase]");
    __dom_chart_BufferStat1 = document.getElementById("chart_BufferStat1");
    __obj_chart_BufferStat1 = new Chart(__dom_chart_BufferStat1, {
        type: 'line',
        data: _ob_chart_database,
        options: _ob_chart_options
    });
    return false;
}
function __fs_pushChartDP(CHART, val) {
    CHART.data.datasets[0].data.push(val);
    CHART.update();
}
function DEBUG__finit_pushChartLabel(val) {
    __obj_chart_BufferStat1.data.datasets[0].data.push(0);
    __obj_chart_BufferStat1.data.labels.push(val);

    //__obj_chart_BufferStat1.data.datasets[1].data.push(0);
    //__obj_chart_BufferStat1.data.labels.push("");
}
function dbg_fv_filter_str(String) {
    for (let i = 0; i < 24; ++i) {
        _wob_pagebuffer.get_ob_offset(i).fint_search(String, 0, false);
    }
}
function dbg_fv_filter() {
    for (let i = 0; i < 24; ++i) {
        _wob_pagebuffer.get_ob_offset(i).fint_filter_bykey(0, false);
    }
}

/* Assign clickhandlers to callback functions*/
function _fb_init_clickhandlers() {
    console.log("[_fb_init_clickhandlers]");
    $('body').on('mouseenter', '.newsElementContainer-box', f_clbk_onhover_ent_nelmcontb);
    $('body').on('mouseleave', '.newsElementContainer-box', f_clbk_onhover_lev_nelmcontb);
    $("div.navbar-tab").on("click", f_clbk_onclick_navelem);
    $('#input_btn_group_siebar > span').click(function (e) {
        console.log(e,  $(this).index());
        _fb_switch_sidebar_view($(this).index());
    });
    $("#input-sidebar-btn").change(function () {
        // this will contain a reference to the checkbox
        console.log("State: ", this.checked);
        if (this.checked) {
            _fb_switch_viewmode(1);
            // the checkbox is now checked 
        } else {
            _fb_switch_viewmode(0);
            // the checkbox is now no longer checked
        }
    });
}

/*Invert an integer, the fact that I need this, is tragic.*/
function fint_invert(In_int_value, In_int_range){
    return ~(In_int_value - In_int_range) + 1;
}

let __b_onclickNavEventLock = false;
var f_clbk_onclick_navelem = function (e) {
   /*if (!__b_onclickNavEventLock & !__b_bodyAnimationLock) {
        __b_onclickNavEventLock = true;
        __b_bodyAnimationLock = true;
    }*/
    if (__b_onclickNavEventLock)
        return false;
    console.log("[f_clbk_onclick_navelem] => CLICK: Navbar: ", e);
    //let d = e.currentTarget.offsetTop.index();
    let d = $(e.currentTarget).index();
    let invert = fint_invert(d, 25);
    if (invert == 0 | invert == 23)
        return false;
    if (invert == 0)
        invert++;
    if (invert == 25)
        invert--;
        
    //var invert = ~(d - 23) + 1;
    //var element = __tableElementBuffer.getByBufferOffset(invert).domcontainer.root;

    let int_offset = _wob_pagebuffer.get_ob_offset(--invert).fob_get_dom().offsetTop;
    //console.log("Lookup: ",  __tableElementBuffer.getByBufferOffset(invert).getDomRoot());
    console.log("\t[f_clbk_onclick_navelem] Offset top: ", int_offset);
    $('html, body').animate(
        {
            scrollTop: int_offset - (invert == 0 ? 100 : 40)
        }, 500, "swing", function () {
            __b_onclickNavEventLock = false;
        //__b_onclickNavEventLock = false;
        //__b_bodyAnimationLock = false;
        }
	);
    return true;
}

//var f_clbk_onhover_ent_nelmcontb = function (e) {
//   console.log("[f_clbk_onhover_ent_nelmcontb] => Mouse enter: ", e);
//}
//var f_clbk_onhover_lev_nelmcontb = function (e) {
//   console.log("[f_clbk_onhover_lev_nelmcontb] => Mouse leave: ", e);
//}


/*
	Various global locks, I'm aware It's terrible design, but eh.
*/

let __dom_elemHoverActiveTarget = null;
let __b_elemhoverLocked = false;
let _tmr_onhoverTimer = null;
let __b_leafpaintLock = false;
let domRight = null;
let domLeft = null;
let domBottom = null;

var f_clbk_onhover_ent_nelmcontb = function(e){
	if(!__b_elemhoverLocked){
		__b_elemhoverLocked = true;
		__dom_elemHoverActiveTarget = e.currentTarget;
		
		//if(!boolpaintLock){			}
		_tmr_onhoverTimer = setTimeout(function(){
			__b_leafpaintLock = true;
			var f = $(e.currentTarget);
			var index = f.index(); 
			
			// Index of target
			var dindex = f.parent().index();
			var dparent = f.parent().parent().parent().parent().parent().parent().parent();
			var pindex = dparent.index();
			
			//TODO: FINISH THIS
		    //let intDrawn = _wob_pagebuffer.get_ob_offset(pindex).drawn();

			var activeTable = _wob_pagebuffer.get_ob_offset(pindex);
			var activeFrame = activeTable.fb_active_frame();
			var activeOb = activeFrame.fob_get(dindex);
			
			console.log("Drawing popup:");
			console.log("\tUsing data index: ", pindex);
			console.log("\tUsing buffer index: ", dindex);
			
			function _dom_generateDisplayTab(strText){
				var dom = document.createElement("div");
				fdom_setText(dom, strText);
				return dom;
			}				
			
			//lol this call, it's the fastest route
			var p = activeOb.dom.firstChild.firstChild.nextElementSibling.nextElementSibling.firstElementChild.currentSrc;
			var rightEdge =  f.offset().left + f.width();
						
			var rightDivEdge = rightEdge + 150;
			var leftEdge = f.offset().left;
			var leftDivEdge = leftEdge - 150;
			console.log("Edge: ", rightDivEdge);
			
			/*
				Leaf on the right
			*/
			
			domRight = document.createElement("div");	
			/*var d1 = _dom_generateDisplayTab("G Key: " + lookupObj.gk);
			domRight.appendChild(d1);
			var d2 = _dom_generateDisplayTab("S Key: " + lookupObj.sk);
			domRight.appendChild(d2);
			var d3 = _dom_generateDisplayTab("Format Key: " + lookupObj.fmt);
			domRight.appendChild(d3);
			var d4 = _dom_generateDisplayTab("SK Name Key: " + lookupObj.gk);
			domRight.appendChild(d4);*/
			
			var d4 = document.createElement("div");
			fdom_setText(d4, "Places:");
			for (let i = 0; i < 0;
			    //activeOb.dob.ner_l.length; 
                i++) {
				let sub = document.createElement("p");
				fdom_setText(sub, i+1 + ". " + lookupObj.data.ner_l[i]);
				d4.appendChild(sub);
			}
			domRight.appendChild(d4);

			var d5 = document.createElement("div");
			fdom_setText(d5, "Organizations:");
			
			//console.log("Input: ", lookupObj.data);
			//console.log("Size: aboutbuf", lookupObj.data.ner_o.length);
			//dom_setAttrib(header_left, "class", "class-name-here");
			for(let i = 0; i < 0;
			    //lookupObj.data.ner_o.length; 
                i++) {
				let sub = document.createElement("p");
				fdom_setText(sub, i+1 + ". " + lookupObj.data.ner_o[i]);
				d5.appendChild(sub);
				
			}
			
			domRight.appendChild(d5);
			var d6 = document.createElement("div");
			fdom_setText(d5, "People:");
			for(let i = 0; i < 0;
			    //lookupObj.data.ner_p.length; 
                i++) {
				let sub = document.createElement("p");
				fdom_setText(sub, i+1 + ". " + lookupObj.data.ner_p[i]);
				d6.appendChild(sub);
				
			}
			domRight.appendChild(d6);
			
			/*
				Leaf on the left
			*/
			domLeft = document.createElement("div");
			var host = "host";
			
			domBottom = document.createElement("div");
			let big_img = document.createElement("img");
			big_img.src = activeOb.dob.thumb;
			domBottom.appendChild(big_img);
			
			fdom_setAttrib(domBottom, "class", "newsElement-slide-fat");
			$(domBottom).appendTo(e.currentTarget);
			
			var rdirection;
			if(rightDivEdge < __int_viewportWidth){
				fdom_setAttrib(domRight, "class", "newsElement-slide-right");
				$(domRight).appendTo(e.currentTarget).animate({
						width: 150,
					}, 250);
			}else{
				console.log("Right clipping!");
				fdom_setAttrib(domRight, "class", "newsElement-slide-bottom");
				$(domRight).appendTo(e.currentTarget).animate({
					height: 280,
				}, 150);
			}
			var ldirection;
			if(leftDivEdge > 0){
				fdom_setAttrib(domLeft, "class", "newsElement-slide-left");
				$(domLeft).appendTo(e.currentTarget).animate({
					right: 153, //153
					opacity: 1,
				}, 250);
			}else{
				console.log("Left clipping!");
				fdom_setAttrib(domLeft, "class", "newsElement-slide-bottom");									
				$(domLeft).appendTo(e.currentTarget).animate({
					height: 280,
				}, 150);
			}
		}, 1000);
	}
}
var f_clbk_onhover_lev_nelmcontb = function(e){
	if(e.currentTarget === __dom_elemHoverActiveTarget){
		if(__b_elemhoverLocked){
			clearTimeout(_tmr_onhoverTimer);
		}
		if(__b_leafpaintLock){
			$(domRight).remove();
			$(domLeft).remove();
			$(domBottom).remove();
			__b_leafpaintLock = false;
		}
								
	}
	__b_elemhoverLocked = false;
}




let __int_tableViewportScrollIndex = 0;
var __int_prevTop = -1;
let __int_viewportScroll_l =0;
let __b_treeFlag = false;
let __int_viewportClamp = 0;

let __int_viewportScroll = 0;
let _b_FlagScrollFixed = false;
function fint_regenerate_margins() {
    let g = $(
        ".content-bar-content-container"
        ).width();

    console.log("Width: %d", g);
    //__int_viewportWidth - 6 - 12
    let f = Math.floor(g / 154); //164 TODO: sort this out
    let d = (g - (156 * f)) / 2;
    console.log("Elements in row: %d / Margin: %d", f, d);
    $(
        ".content-bar-content"
        //'.news-group-table'
        ).each(function (e) {
            //Calculate sizing
            $(this).css('margin-left', d + "px");
        });
    return f;
}

// On resize fire this
$(window).resize(function (e) {
    console.log("[Window event] -> Resize :", e);
    __int_viewportClamp = fint_regenerate_margins();

    _ob_screen.int_rs_viewportWidth = fint_window_width();
    _ob_screen.int_rs_viewportHeight = fint_window_height();

    if (_ob_screen.int_rsl_viewportWidth < _ob_screen.int_rs_viewportWidth) {
        //March object tables, because the viewport is larger, thus, some objects might be visible
        console.log("Larger!");
    }

    console.log("\tScreen: ", _ob_screen);
    console.log("\tClamp : %d", __int_viewportClamp);

    _ob_screen.int_rsl_viewportWidth = _ob_screen.int_rs_viewportWidth;
    _ob_screen.int_rsl_viewportHeight = _ob_screen.int_rs_viewportHeight;
});

$(window).on("scroll", function (e) {
    __int_viewportScroll = $(window).scrollTop();
    if (__int_viewportScroll > 10) {
        let it = 0;
        //console.log("Tree flag: ", (__b_treeFlag) ? "Lower" : "Upper");
        /*if (__int_viewportScroll_l <= __int_viewportScroll) {
            //Go up the table
            //if(__b_treeFlag) //Bottom half of the table last time
        } 
        __int_viewportScroll_l = __int_viewportScroll;
        *//*
        for (let i = __int_tableViewportScrollIndex; i < 24; ++i) {
            let g = _wob_pagebuffer.get_ob_offset(i);//.fob_get_dom();

            if (g == null)
                continue;

            //console.log("Item: ", g.getDomRoot());
            //if(g == null){
            //	console.log("Minifier broke");
            //}
            let r = fb_boundinginviewport(g.fob_get_dom());
            if (r) {
                __int_tableViewportScrollIndex = i;
            }
            //console.log("Result: ", r);
            //If result == true, and it < 4 
            if (r & it < 2) {//console.log("In viewport: ", g);
                if (it == 0 & __int_prevTop != i) {
                    //$(g.getDomRoot()).addClass("c-anim-fade-out");
                    __int_prevTop = i;
                    //onsole.log("Updated");
                    //console.log("Time: ", g.time().getHours());
                    $("#navbar-time-output").text(fstr_timeDateShort(g.time()));
                    //_nav_HighlightElement(~(i - 23) + 1, "#FFFFF")
                }
                ++it;
            }
        }*/
    }
    if (__int_viewportScroll > 60) {
        if (!_b_FlagScrollFixed) {
            _b_FlagScrollFixed = true;
            $(__dom_navbar_container).addClass("navbar-m-fix");
        }
    } else {
        _b_FlagScrollFixed = false;
        $(__dom_navbar_container).removeClass("navbar-m-fix");
    }
});

let __wob_nav_progressbar;

//let navbuf = new CircularBuffer(25);
let __wob_a_navElementLookup = new Array(24);

function _fv_uinav_UpdateElementPopupText(intIndex, strText) {
    let intIdx= ~(intIndex - 23) + 1;
    $(__wob_a_navElementLookup[intIdx].ob_domref).children().eq(1).text(strText);
}
function _ui_UpdateMicroFront(strString) {
    __wob_a_navElementLookup[23].ob_domref.innerHTML = strString;
}

function _nav_UpdateLookup(varTopHour) {
    console.log("_nav_UpdateLookup");
    var adjust = (varTopHour - 23);
    var time;
    for (let i = 0; i < 24; ++i) {
        if ((adjust + i) < 0) {
            time = ((adjust + i) + 24);
        } else {
            time = (adjust + i);
        }
        if (time < 10) {
            time = ("0" + time + ":00");
        } else {
            time = (time + ":00");
        }
        DEBUG__finit_pushChartLabel(time);
        $(__wob_a_navElementLookup[i].ob_domref).children().eq(0).text(time);
    }
}
function _fb_ui_nav_init(In_Time) {
    let intTime = In_Time.getHours();
    console.log("_nav_initLookup");

    __wob_nav_progressbar = $("#nav-progressbar").progressbar({
        value: 1
    });

    var realtime = (intTime - 24);
    for (let i = 0; i < 24; ++i) {
        var object = document.getElementById("n" + (24 - i));
        //console.log("Allocated: %d @: ",i,  object);
        if (object == null) {
            coreLog("Undefined document element, fatal error , failfast");
            return false;
        }
        __wob_a_navElementLookup[i] = {
            ob_domref: object,
        };//_buildNavObject(0, object);
    }
    return true;
}






/*
	PAGE ANIMATION, standard animation speed in MS

*/
const PAGE_ANIMATION = 150;
const VIEWMODE_FULL = 0;
const VIEWMODE_SIDEBAR_HALF = 1;
const VIEWMODE_SIDEBAR_WIDE = 2;

const SIDEBAR_VIEW_SEARCH = 0;
const SIDEBAR_VIEW_GRAPH = 1;
const SIDEBAR_VIEW_SETTINGS = 2;

// On boot, init to search view tab
let __int_viewmode = 0;
let __int_sidebar_viewmode = SIDEBAR_VIEW_SEARCH;

function _fb_switch_sidebar_view(In_int_mode) {
    if (In_int_mode == __int_sidebar_viewmode)
        return false;
    console.log("[_fb_switch_sidebar_view]");

    $(__a_dom_data_sidebar[__int_sidebar_viewmode]).hide();
    $(__a_dom_data_sidebar[In_int_mode]).fadeIn(200);

    switch (In_int_mode) {
        case SIDEBAR_VIEW_GRAPH:
            break;
        case SIDEBAR_VIEW_SETTINGS:
            break;
        case SIDEBAR_VIEW_SEARCH:
            break;
    }
    __int_sidebar_viewmode = In_int_mode;
    return true;
}

// Toggle viewmode
function _fb_toggle_viewmode() {
	//NOT USED
	return false;
}

// Swich viewmode to requested mode.

function _fb_switch_viewmode(In_int_mode) {
    if (In_int_mode == __int_viewmode)
        return false;
    switch (In_int_mode) {
        case VIEWMODE_FULL:
            console.log("VIEWMODE_FULL");
            /*$(__dom_data_main_wrapper).width("100%");
            $(__dom_data_sidebar_wrapper).hide();*/
            $(__dom_data_main_wrapper).css({width: "100%"});/*.animate(
               {
                   width: "100%"
               }, PAGE_ANIMATION
            );/**/
            $(__dom_data_sidebar_wrapper).hide();/*.animate(
               {
                   width: 0
               }, PAGE_ANIMATION, function () { $(this).hide(); }
            );/**/
            break;
        case VIEWMODE_SIDEBAR_HALF:
            console.log("VIEWMODE_SIDEBAR_HALF");
           /* $(__dom_data_main_wrapper).width("80%");
            $(__dom_data_sidebar_wrapper).width("20%");
            $(__dom_data_sidebar_wrapper).show();*/

            $(__dom_data_main_wrapper).css({width: "70%"});/*.animate(
			    {
			        width: "70%"
			    }, PAGE_ANIMATION
		    );/**/
            $(__dom_data_sidebar_wrapper).css({ width: "30%" }).show();/*animate(
              {
                  width: "30%"
              }, PAGE_ANIMATION
           );/**/
            break;
        case VIEWMODE_SIDEBAR_WIDE:
            console.log("VIEWMODE_SIDEBAR_WIDE");
            $(__dom_data_main_wrapper).width("70%");
            $(__dom_data_sidebar_wrapper).width("30%");
            $(__dom_data_sidebar_wrapper).show();
            break;
    }
    __int_viewportClamp = fint_regenerate_margins();
    __int_viewmode = In_int_mode;
    return true;
}


/*
	CircularBuffer
	ARGUMENTS: NONE
	RETURNS: OBJECT BUFFER
	Circular buffer factory, acts as a ring buffer, with fixed storage space.
	In this case, it's used to buffer 24 buffers, and cycles around every hour.
*/
function CircularBuffer(intSize) {
    var _offset = 0;
    var _buffersize = intSize; //24 elements 0 indexed
    var __alloc_size = intSize + 1;
    var _a_allocbuf = Array(__alloc_size);
    var _int_head = 0;
    var _int_tail = 0;
    var _int_last;
    var _int_idx;


    //this.init = function () { console.log("init"); }
    console.log("[CircularBuffer] loaded with: %d", intSize);
    for (let i = 0; i < __alloc_size; ++i) {_a_allocbuf[i] = null; }
    this.fint_buffersize = function () { return _buffersize - 1; }
    this.fint_allocsize = function () {return __alloc_size;}
    this.fint_size = function () {return _buffersize;}
    this._get_tail = function () {
        if (_offset == 0) {
           _int_idx = __alloc_size - (_buffersize - 1);
        } else if ((_offset - (_buffersize - 1)) < 0) {
            var r = (_offset - (_buffersize - 1));
           _int_idx = __alloc_size + r;
        } else {
           _int_idx = _offset - (_buffersize - 1);
        }
        return _int_idx;
    }
    this._dealloc = function (_idx) {
        if (_a_allocbuf[_idx] != null) {
            console.log("[CircularBuffer: _dealloc]");
            _a_allocbuf[_idx] = null;
        }
    }
    this.getpos = function () {
        return _offset;
    }
    this.prev = function () {
        return _int_last;
    }
    this._alloc = function (_idx, data) {_a_allocbuf[_idx] = data; }
    this.print_headtail = function () {var back = _a_allocbuf[this._get_tail()]; var front = _a_allocbuf[_offset];}
    this.get_ob_offset = function (index) {
        if (index > 24)
            return false;
        if ((_offset - index) < 0) {
            var r = (_offset - index);
            var off = ((_buffersize - 1) + r);
            return _a_allocbuf[off];
        } else {
            return _a_allocbuf[_offset - index];
        }
        return _a_allocbuf[_offset];
    }
    this.get_back = function () {var elem = this._get_tail();  return elem == null ? false : elem; }
    this.push_front = function (data) {
        if (++_offset < __alloc_size) {
            this._alloc(_offset, data);
        } else {
            _offset = 0;
            this._alloc(_offset, data);
        }
        var t = this._get_tail();
        _int_last = _a_allocbuf[t];
        this._dealloc(t);
        _int_head = _offset;
    }
    this.get_front = function () { return _a_allocbuf[_offset]; }
    this.front__idx = function () { return _offset; }
    this.dump = function () {
        console.log("[CircularBuffer: dump]");
        console.log("\tBuffer: ", _a_allocbuf);
        console.log("\tBuffer dump\n\tFront: (%d)", _offset);
        var_int_idx = 0;
        console.log("\tBack: (%d)",_int_idx);
        console.log("\tLength (%d)", _a_allocbuf.length);
        for (i = 0; i < __alloc_size; ++i) {
            console.log("\t\tIndex: %d", i, _a_allocbuf[i]);
        }
    }
    this.print = function () {
        console.log("[CircularBuffer: status]\n\tSize: %d\n\_buffersize: %d\n\tAlloc Size: %d\n\tOffset: %d\n",
		__alloc_size,
		_buffersize,
		_a_allocbuf.length,
		_offset);
    }
    return this;
}
var _wob_pagebuffer = new CircularBuffer(25);
function _fv_init_pagebuffer() { /*Load element table with 24 tables and unallocated element buffers*/
    console.log("[_fv_init_pagebuffer]");
    scroll(0, 0);
    for (let i = 23; i >= 0; --i) {
        let obd = new Date();
        obd.setHours(_wob_ltime.getHours() - i);
        console.log("[_fv_init_pagebuffer] HOUR: ", obd.getHours());

        let t = new fob_pageBuffer(obd);
        _wob_pagebuffer.push_front(t);
        //console.log("Buffer: ", t.fob_get_dom());

        let p = _fob_build_ContainerWrapperDom();//.appendChild(t.fob_get_dom());
        p.appendChild(t.fob_get_dom());
        console.log("Final: ", p);
        $(p).prependTo(__wdom_dataContainer_DomGrid);
        //_fob_build_ContainerWrapperDom().appendChild(t.fob_get_dom());
        //$( _fob_build_ContainerWrapperDom().appendChild(t.fob_get_dom()));//.hide().prependTo(__wdom_dataContainer_DomGrid).fadeIn(500);
    }
}
function _fv_init_storage() {
    console.log("[_fv_init_storage] Loading storage structure\n");
    _fv_init_pagebuffer();
	
}
/*
    Circular buffer - RAW
        V
       pageBuffer  - private: fob_tableBuffer
              V
              elementBuffer - private: fob_tableBuffer
*/

/*
	fob_tableBuffer	
	ARGUMENTS: NONE
	RETURNS: OBJECT BUFFER
	General purpose buffer factory used in various objects
*/
function fob_tableBuffer(In_intMaxSz) {
    let _int_sz = 0;
    let _int_maxsz = In_intMaxSz;
    let __aob_tableArray = Array(0);
    console.log("\t\t[fob_tableBuffer] Constructed table buffer (%d)\n", _int_maxsz);
    this.fv_push_front = function (int_ob) {
        _int_sz++;
        __aob_tableArray.push(int_ob);
    }

    /* fv_push_rear: Returns true if array is greater than zero
    this.fv_push_rear = function(){
        __aob_tableArray.splice(0, 0, objData);
        return true;
    }*/
    this.fint_size = function () { return _int_sz; }
    this.fint_delete_at = function (intIndex) { __aob_tableArray.splice(intIndex, 1); }
    this.fob_get_at = function (int_Index) {
        if (int_Index < 0 | int_Index > _int_sz) alert("WARNING EXCEPTION IN TABLE BUF, " + _int_sz);
		
        return __aob_tableArray[int_Index];
    }
    this.fv_prettyprint = function () {
        console.log("Dumping array: ");
        for (let i = 0; i < _int_sz; ++i) {
            console.log("\tElement: ", __aob_tableArray[i], __aob_tableArray.size());
        }
    }
    return this;
}

const TABLE_MAX_SIZE = 30;

/*
	fob_elementBuffer	
	ARGUMENTS: NONE
	RETURNS: OBJECT BUFFER
	
	Buffer object factory for parent table buffers, 
	stores a buffer of constructed dom objects, 
	the raw table corresponding to it, and functions to search and visually manipulate the view
*/
function fob_elementBuffer(intNamedIndex) {
    let _ob_domref = _fob_build_elementContainerTableDom(intNamedIndex);
	
    this.fob_get_dom = function () {
        return _ob_domref;
    }
    console.log("\t[fob_elementBuffer] Generating fob_elementBuffer buffer");
	
	// Generate enternal buffer
    let _ob_ebuf = new fob_tableBuffer(TABLE_MAX_SIZE);

    this.fob_get = function(In_int_Index){
        return _ob_ebuf.fob_get_at(In_int_Index);
    }
    this.push = function (In_ob
        //,In_draw_flag
        ) {
        let ob_dom = _fob_build_NewsElementDOM(
            fstr_getLookupStr( In_ob.rak, In_ob.rsk),
            //In_ob.rsk,
            In_ob.rgk,
            "add",
            "",
            In_ob.title,
            In_ob.descr,
            In_ob.link,
            In_ob.pubd
            );

        let ob_store_package = {
                dom: ob_dom,
                dob: In_ob
            };

        /*Add DOM*/
        //if (In_draw_flag)
            //$(_ob_domref).prepend(ob_dom);
		
		//Lazy hack, if prepend is supported by the javascript engine, we'll use that as it's faster,
		// else, call jquery
        try {
            _ob_domref.prepend(ob_dom);
        } catch (e) {
            $(_ob_domref).prepend(ob_dom);
        }
        /*Store packed object*/
        _ob_ebuf.fv_push_front(ob_store_package);
        //_ob_ebuf[0].fv_push_front(In_ob);
    }
    this.size = function () {
        return _ob_ebuf.fint_size();
    }
}

const ELEMENT_ROW_MULTIPLIER = 2;
let __wint_focuscnt = 0;
let PBUF_MAX_SIZE = 30;


/*
	fob_pageBuffer	
	ARGUMENTS: NONE
	RETURNS: OBJECT BUFFER
	Buffer object factory for page views, 
	stores a buffer of constructed dom objects, 
	the raw table corresponding to it, and functions to search and visually manipulate the view
	
*/
function fob_pageBuffer(
    //In_intTime
    In_ob_Time
    ) {
    console.log("[fob_pageBuffer] Constructed page buffer: Generating first buffer\n")
    let _ob_buf = new fob_tableBuffer(PBUF_MAX_SIZE);//(In_intSz, In_intTime);
    //let _int_Time = In_intTime; 
    let _ob_Time = In_ob_Time;
    let active = 0;
    
    this.time = function(){
        return _ob_Time;
    }

    let _b_flagSearch = false;
    var _flag_array = new Array();
    this.fint_clear_search = function () {
        if (_b_flagSearch)
        for (let i = 0; i < _flag_array.length; ++i) {
            $(_flag_array[i].dom).css({
                transform: "none",
            }).fadeTo(500, 1);
        }
        _b_flagSearch = false;
    }

    this.fint_filter_bykey = function (In_int_key, In_int_ShowFlags) {
        let _int_cnt = 0;
        this.fint_viewmode(2);
        for (let t = 0; t < _ob_buf.fint_size() ; ++t) {
            let ob_tbl = _ob_buf.fob_get_at(t).array;   //.fob_get(t);
            for (let i = 0; i < ob_tbl.size() ; ++i) {
                var ob_ref = ob_tbl.fob_get(i);

                //Assign search query to add element sorter
                var delegate = function () {

                }
                if (ob_ref.dob.rsk == In_int_key) {
                    _int_cnt++;

                    $(ob_ref.dom).find("img").attr('src', function (_i, src) {
                        if (ob_ref.dob.thumb.length == 0)
                            return src = "/assets/img/error/404-grey.png";
                        else
                            return src = ob_ref.dob.thumb

                    });

                    $(ob_ref.dom).appendTo(_ob_domref.scontent).fadeIn(500);


                } else {
                    $(ob_ref.dom).fadeTo(500, 0.2, function () {
                        if (In_int_ShowFlags == true) {
                            //$(this).hide();
                        } else {
                            $(this).css({
                                //transform: "scale(0.9, 0.9)",
                                transition: "0.3s"
                            });
                        }
                    });
                }
            }
        }


        return _int_cnt;
    }

    let _int_swrapper_scrolllock = false;
    let _int_scroll = 0;
    let _int_swrapper_scroll_idx = 0;
    this.dbfint_ui_filterscroll = function (In_bool_direction) {
        _int_swrapper_scrolllock = true;
        let p =
        $(_ob_domref.swrapper).animate({
            scrollTop: 287 * ((In_bool_direction) ?
                ++_int_swrapper_scroll_idx
                : --_int_swrapper_scroll_idx)
        }, PAGE_ANIMATION * 2, "swing", function(){
            if (p.scrollTop() !=
            (_int_scroll + (287 * _int_swrapper_scroll_idx))
            )
                console.log("Did not scroll!");
            _int_scroll = p.scrollTop();
            _int_swrapper_scrolllock = false;
        });
       
       
        return _int_swrapper_scroll_idx;
    }

    /*Search description and title elements for matching REGEX*/
    this.fint_search = function (In_str_Text, In_int_Flags, In_int_ShowFlags) {
        //if (_b_flagSearch)
        //    return false;
        let _int_cnt = 0;
        this.fint_viewmode(2);
        _b_flagSearch = true;
        for (let t = 0; t < _ob_buf.fint_size() ; ++t) {
            let ob_tbl = _ob_buf.fob_get_at(t).array;   //.fob_get(t);
            let intRet;
            for (let i = 0; i < ob_tbl.size(); ++i) {
                var ob_ref = ob_tbl.fob_get(i);
                switch (In_int_Flags) {
                    case 0:
                        intRet = ob_ref.dob.title.search(In_str_Text);
                        break;
                    case 1:
                        intRet = ob_ref.dob.descr.search(In_str_Text);
                        break;
                    default:
                        console.log("Bad selector?!");
                        continue;
                        break;
                }
                //bad result = skip
                if (intRet < 0
                    //& In_int_ShowFlags == 0
                    | ob_ref.dob.rsk == 4
                    ) {
                    $(ob_ref.dom).fadeTo(500, 0.2, function () {
                        if (In_int_ShowFlags == 0) {
                            $(this).hide();
                        } else {
                            $(this).css({
                                //transform: "scale(0.9, 0.9)",
                                transition: "0.4s"
                            });
                        }
                        
                    });
                    _flag_array.push(ob_ref);
                        //fadeOut(500);
                } else {
                    _int_cnt++;
                    /*$(ob_ref.dom).css({
                        transform: "scale(1.1, 1.1)",
                        transition: "0.4s"
                    });*/
                    $(ob_ref.dom).find("img").attr('src', function (_i, src) {
                        if (ob_ref.dob.thumb.length == 0)
                            return src = "/assets/img/error/404-grey.png";
                        else
                            return src = ob_ref.dob.thumb

                    });
                    $(ob_ref.dom).appendTo(_ob_domref.scontent).fadeIn(500);

                }
                //else if (t == active - 1)
                //continue;
            }
        }
        return _int_cnt;
    }

    const PBUF_VIEWMODE_FULL = 0;
    const PBUF_VIEWMODE_SPLIT = 1;
    const PBUF_VIEWMODE_SIDEBAR = 2;
    let _int_viewmode = 0;
    this.fint_viewmode = function (In_int_viewmode) {
        if (In_int_viewmode == _int_viewmode)
            return false;
        switch (In_int_viewmode) {
            case PBUF_VIEWMODE_FULL:
                $(_ob_domref.mwrapper).show().animate({
                    width: "100%"
                }, PAGE_ANIMATION);
                $(_ob_domref.swrapper).animate({
                    width: "0%",
                    "max-width": "0%"
                }, PAGE_ANIMATION, function () { $(this).hide()});
                break;
            case PBUF_VIEWMODE_SPLIT:
                console.log("PBUF_VIEWMODE_SPLIT");
                $(_ob_domref.mwrapper).animate({
                    width: "80%"
                }, PAGE_ANIMATION);
                $(_ob_domref.swrapper).animate({
                    width: "20%",
                    "max-width": "calc(20%-20px)"
                }, PAGE_ANIMATION, function () { $(this).show();});

                break;
            case PBUF_VIEWMODE_SIDEBAR:
                //$(_ob_domref.mwrapper).hide();
                $(_ob_domref.mwrapper).hide()
                    /*.animate({
                    width: "20%",
                }, PAGE_ANIMATION);*/
                $(_ob_domref.swrapper).show().animate({
                    width: "100%",
                    "max-width": "calc(100%-20px)"
                }, PAGE_ANIMATION);
                break;
            default:
                return false;
                break;
        }
        _int_viewmode = In_int_viewmode;
        return true;
    }
	this.fb_active_frame = function(In_intIndex){
		return _ob_buf.fob_get_at(active).array;
	}
	this.fb_pick = function (In_intIndex){
		return null;
	}
    this.fb_focus = function (In_intIndex, In_DRAW_FLAG) {
        console.log("[fob_pageBuffer] : [fb_focus]");
        if (In_intIndex == active
            & _ob_buf.fob_get_at(In_intIndex).flagDrawn
            // & _ob_buf.fint_size() > 1
            ) {
            return false;
        }
        __wint_focuscnt++;

        /*Fire search function on new window to quick sort it*/
        if (_b_flagSearch) {
            this.fint_search("Trump", 0, 0);
        }


        $(_ob_buf.fob_get_at(active).array.fob_get_dom()).hide();
        $(_ob_buf.fob_get_at(In_intIndex).array.fob_get_dom()).show();
        active = In_intIndex;

        if (In_DRAW_FLAG) {
            console.log(In_DRAW_FLAG);
        }
        if (In_DRAW_FLAG & !_ob_buf.fob_get_at(In_intIndex).flagDrawn) {
            //Set LOCK FLAG, so images cannot be reallocated to their container
            _ob_buf.fob_get_at(In_intIndex).flagDraw = true;

            let pbuf = _ob_buf.fob_get_at(active).array;
            
            for (let i = 0; i < pbuf.size() ; ++i) {
                let ob = pbuf.fob_get(i);

                $(ob.dom).find("img").attr('src', function (_i, src) {
                    if (ob.dob.thumb.length == 0)
                        return src = "/assets/img/error/404-grey.png";
                    else
                        return src = ob.dob.thumb
                    
                });
            }
        }

       
        //_ob_buf.fob_get_at(In_intIndex).array.fob_get_dom();
        return true;
    }
    //Dom reference
    _ob_buf.fv_push_front({
        flagDrawn: false,
        array: new fob_elementBuffer(0)
    });

    /*console.log("In: ", (_int_Time.toDateString() +
       ": " + _int_Time.getHours() +
       ":00"))*/
    let _ob_domref = _fob_build_elementContainerDom(  
       (_ob_Time.toDateString() +
       ": " + _ob_Time.getHours() +
       ":00"), false);

    $(_ob_buf.fob_get_at(active).array.fob_get_dom()).prependTo(_ob_domref.mcontent);//.show();

    this.fob_get_dom = function(){
        return _ob_domref.root;
    }
   
    /*let tob = {
        flagDrawn: false,
        array: new fob_elementBuffer()
    };*/
    //console.log("Object: ", tob);
    //_ob_buf.fv_push_front(new fob_elementBuffer());

    this.push_front = function (In_ob) {

        //If size < MAX_SIZE, PUSH ONTO FRONT OBJECT
        if (_ob_buf.fob_get_at(
                _ob_buf.fint_size() - 1
            ).array.size() - 0
            > PBUF_MAX_SIZE) { //Push
            console.log("[fob_pageBuffer] Generating new buffer, as previous is full\n");
            //console.log("object: ", tob)
            _ob_buf.fv_push_front(
                {
                flagDrawn: false,
                array: new fob_elementBuffer(_ob_buf.fint_size())
                }
            );

            //$(_ob_domref).append(
            //  _ob_buf.fob_get_at(_ob_buf.fint_size() - 1).array.fob_get_dom()
            //  );
            //new fob_elementBuffer());
            // _ob_buf.fint_size() - 1).array.fob_get_dom().root
            $(
                _ob_buf.fob_get_at(_ob_buf.fint_size()-1).array.fob_get_dom()

                ).hide().appendTo(_ob_domref.mcontent);

            _ob_buf.fob_get_at(_ob_buf.fint_size() - 1).array.push(In_ob);
            //this.fb_focus(_ob_buf.fint_size()-1, false);

        } else {
            _ob_buf.fob_get_at(
                _ob_buf.fint_size() - 1
            ).array.push(In_ob);
            //_ob_buf.PUSH(In_ob);
        }
    }
    this.fv_debug_pretty_print = function(){
        for (let i = 0; i < _ob_buf.fint_size() ; ++i) {
            console.log("[fv_debug_pretty_print] Dumping element buffer\n");
            _ob_buf.fv_prettyprint();
        }
    }
    this.size = function () {
        return _ob_buf.fint_size();
    }
    return this;
}


/* MAIN ENTRY*/
let _ob_time;
let _intTimezone;
let _ob_screen;
function fs_geturiquery() {
    console.log("[f_geturiquerystr]: ", window.location.href);
    return window.location.href;
}

/* FUNCTION fob_uritob

	SUPPLIED: VOID
	RETURNS: object
	Turn query string into javascript array.
*/
function fob_uritob() {
    var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query = window.location.search.substring(1);

    let urlParams = {};
    urlParams.h = window.location.origin
   // query.slice(0, query.indexOf("?"));
    while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);

    console.log("[_furlitobj]: ", window.location.href, urlParams);
    return urlParams;
}
function _fb_initcore(){
	console.log("[_fb_initcore]");
	_ob_screen = new Object();
	
	// Store screen measurements on startup.
	_ob_screen.int_viewportWidth 	= fint_window_width();
	_ob_screen.int_viewportHeight 	= fint_window_height();
	_ob_screen.int_screenX  		= fint_screen_width();
	_ob_screen.int_screenY 			= fint_screen_height();
	
	console.log("DISPLAY:\n\tScreen width: %d\n\tViewport width: %d", _ob_screen.int_screenX, _ob_screen.int_viewportWidth);

	// Spout some helpful nonsense
	let ytab = _ob_screen.int_screenX - _ob_screen.int_viewportWidth;
	let xtab = _ob_screen.int_screenY - _ob_screen.int_viewportHeight;
	console.log("Lost space :( Y[%d] Y[%d]", xtab, ytab);
	
	console.log("Codename: [%s]\nBrowser: [%s]\nVersion: [%s]\nCookies? [%s]\nBrowser language: [%s]\nOnline: [%s]\nPlatform: [%s]\nUser-Agent: [%s]\n", 
		navigator.appCodeName, 
		navigator.appName, 
		navigator.platform,  
		navigator.cookieEnabled, 
		navigator.language, 
		navigator.onLine,
		navigator.platform,
		navigator.userAgent
	);

	// Calculate the number of objects we can fit in a table grid
	// Screenwidth - 0 / width_of_object * number of rows
	PBUF_MAX_SIZE = Math.floor((_ob_screen.int_screenX
        - 0//40
        ) / 154) * ELEMENT_ROW_MULTIPLIER;

	// Floor the variable
	console.log("Clamping to %d", PBUF_MAX_SIZE);   

	// Get time at startup
	_ob_time = new Date();
	_intTimezone = _ob_time.getTimezoneOffset();
	console.log("Date:\n", _ob_time);
    
	console.log("Timezone:", _intTimezone);
	
	//Abnormal timezone, complain about it.
	if (_intTimezone > -10 || _intTimezone > 10) {
	    console.log("\tCannot access timezone object!");
	}
	
	// Parse URL query
	fs_geturiquery();
	fob_uritob();
	console.log("[_fb_initcore] end");
}
function fb_init(){

	// Tell the console where we are.
	console.log("[fb_init]");
	
	// Start core variables
	_fb_initcore();
	
	// Init clock with various lifetime objects
	__fb_internal_clock_init();
	
	// Init circular buffer
	_fv_init_storage();
	
	// Fire up document input handlers for mouse movement and keypresses
	_fb_init_clickhandlers();

	// Regenerate page margins for the first time
	__int_viewportClamp = fint_regenerate_margins();

	// Load nav bar, this is really lazy code, hacked out of the old version
	_fb_ui_nav_init(_wob_ltime);
	
	// Fire up the chart library
	_fb_init_chartBase();

	_nav_UpdateLookup(_wob_ltime.getHours());
    /* Top stop a jarring update in the UI, draw the time before the loop starts*/
	_ui_UpdateMicroFront(fstr_timePadded(_wob_ltime));

	// Fire up clock
	__fb_internal_clock_Start();
    console.log("[fb_init] End")
}

function __fob_runobject() {
    let ob = new Object();
    ob.b_runFlag = false;
    return ob;
}
/*
	Debugging server request
	tmzo: 	Timezone,
	rgid: 	Last response offset in request groups.
	uid: 	Unique ID: 0
	req: 	Request object
	head: 	Request type.
	tkey: 	Tick value, for debugging.
	
*/
let __str_djrqst = '{"tmzo":0,"rgid":0,"tid":0,"uid":0,"req":[{"a":[]},{"a":[]}],"head":"PULL","tkey":0}';
let __str_url = "/ajrq/echo.php"
function __fb_ajaxQuery() {
    $.ajax({
        url: __str_url,
        method: 'POST',
        timeout: 10000,
        data: __str_djrqst /*{
            req: __str_djrqst,
            name: "0F0F0F",
            id:"FFFFFFFFFFFFFF"
        }*/
    })
.then(
    function success(data, name) {
        console.log("[__fb_ajax runner] sucess");
        //if (name !== newName) { }
        // alert('Something went wrong.  Name is now ' + name);
       
        console.log("\tresponseText", data.responseText);
        console.log("\tData: ", data);
        console.log("\tName: ", name);
        console.log("\tStatus text", status);

        console.log("+++ Header:");
        console.log("Element count: (%d)", data.ecnt);
        console.log("Group count: (%d)", data.gcnt);
        console.log("RGID: (%d)", data.rgid);
        console.log("Tick count: (%d)s", data.tcnts);
        console.log("Tick count: (%d)m", data.tcntm);
        let jsonData = data.array;

        if (data.array == null ||
            jsonData.length == 0 ||
            jsonData.length > 24) {
            console.log("Dataset size is 0");
			// Array length is 0, or larger than expected
			//Return fault
            return false;
        }

        let p = $(__wob_nav_progressbar).progressbar({ value: 60 });
        let t = p[0].firstChild;
        t.style.backgroundColor = "#B294BB";
        
        ddispatch(jsonData);
        return true;
    },
    function fail(data, status){
        console.log("[__fb_ajax runner] failure");
        console.log("\tData", data);
        console.log("\tStatus text", status);
        console.log("\tresponseText", data.responseText);
        console.log("\tResponse headers", data.getAllResponseHeaders())
        switch (data.status) {
            case 400:
                console.log("400 BAD REQUEST")
                break;
            default:
                console.log("Unhandled code: %d", data.status);
                break;
        }
        let p = $(__wob_nav_progressbar).progressbar({ value: 100 });
        let t = p[0].firstChild;
        t.style.backgroundColor = "#CC6666";
        //alert('Request failed.  Returned status of ' + status);
        return false;
    }
    );


    let p = $(__wob_nav_progressbar).progressbar({ value: 30 });
    let t = p[0].firstChild;
    t.style.backgroundColor = "#81A2BE";
}


/*
    __ob_inter_clock_timer
    Clock object for __fb_internal_clock
*/
let _wob_time;
let _wob_ltime;
let _b_in_clockFlag = false;
let _b_out_clockFlag = false;
let _wint_runtick = 0;
let __ob_inter_clock_timer;
let _int_clocktick = 1000;

let __ob_dataconsumeob;

// Object locks, WILL be moved to a javascript object to enforce exclusivity.
let __int_mcnt = 0;
let __b_flag_request = false;
let __b_flag_dataconsume = false;
let __b_flag_OUT_consumelock = false;
let __ob_dispatch = null;


// Poorly named function because I was being lazy at the time.
function ddispatch(input) {

	// Get top index
    let intIndex = 0;
    
	// There are some errors here, a run condition will occur if the table is shunted backwards while inserting.
	// Fix? add the lookup for the table top time each loop.
	
	let ob_topdate = _wob_pagebuffer.get_ob_offset(0).time();
    for (let i = 0; i < input.length; ++i) {
        let a_cache = input[i].array;

        //Translate SQL date to JSON internal date
        let ob_date = fob_sqltojDate(input[i].date);
        console.log("Translated date: ", ob_date);

        //Calculate difference between top of table and group in hours
        let topDiff = Math.abs(ob_topdate - ob_date) / 36e5;
        console.log("Selector difference: ", topDiff);

		// That doesn't sound quite right, close immediately and debug server
        if (topDiff < 0 && topDiff > 24){
            alert("SERVER RESPONSE FAULT");
			return false;
		}

        if (topDiff < 1)
            intIndex = 0;
        else
            intIndex = Math.floor(topDiff);

        console.log("[SELECTING: %d]", intIndex);
        /*Cache page buffer*/
        let pbuf = _wob_pagebuffer.get_ob_offset(intIndex);
	
		if(pbuf == null) {
			console.log("Rounding errors, please debug");
			continue;
		}
		
        console.log("Number of elements: ", a_cache.length);
        
        if (a_cache.length > 30) {
            console.log("Hmm");
        }

        for (let e = 0; e < a_cache.length; ++e) {
            pbuf.push_front(a_cache[e]);
        }

        _fv_uinav_UpdateElementPopupText(i, a_cache.length);
        __obj_chart_BufferStat1.data.datasets[0].data[
            ~(i - 24)+1
        ] = a_cache.length;

        pbuf.fb_focus(pbuf.size()-1, true);
        console.log("Resulting size: ", pbuf.size());
    }

    let p = $(__wob_nav_progressbar).progressbar({ value: 100});
    let t = p[0].firstChild;
    t.style.backgroundColor = "#B5BD68";

    //$(__wob_nav_progressbar).progressbarValue.css({ color: "red" });
    __obj_chart_BufferStat1.update();
}
function __fv_internal_clock() {
    _wob_time = new Date();
    ++_wint_runtick;
    if (_wob_time != _wob_ltime) {
        if (_wob_time.getDay() > _wob_ltime.getDay()) {
            console.log("[__internal_clock] New Day");
        }
        if (_wob_time.getHours() > _wob_ltime.getHours()) {
            console.log("[__internal_clock] New Hour");

            let ftime = new Date();
            ftime.setHours((_wob_time.getHours()) - _wob_ltime.getHours());
            let _int_difference = ftime.getHours();
            console.log("Init new page: (%d): ", _int_difference);
            for (let i = 0; i < _int_difference; ++i) {
                let obd = new Date();
                obd.setHours(_wob_time.getHours() - i);

                let t = new fob_pageBuffer(obd);
                _wob_pagebuffer.push_front(t);

                let p = _fob_build_ContainerWrapperDom();
                p.appendChild(t.fob_get_dom());

                $(p).prependTo(__wdom_dataContainer_DomGrid).fadeIn(500);

                __obj_chart_BufferStat1.data.datasets[0].data.splice(0, 1);
                __obj_chart_BufferStat1.data.datasets[0].data.push(0);
            }

        }
        if (_wob_time.getMinutes() > _wob_ltime.getMinutes()) {
            //console.log("[__internal_clock] New Minute");
            if (!__b_flag_OUT_consumelock & __b_flag_request) {
                if (++__int_mcnt > 5) {
                    __int_mcnt = 0;
                    __fb_ajaxQuery();
                }
            }
        }
        if (_wob_time.getSeconds() > _wob_ltime.getSeconds()) {
            //console.log("[__internal_clock] New Second");
            _ui_UpdateMicroFront(fstr_timePadded(_wob_time));
        }
    }
    _wob_ltime = _wob_time;
    if (_b_in_clockFlag) { __ob_inter_clock_timer = setTimeout(__fv_internal_clock, _int_clocktick); } else { _b_out_clockFlag = false; }
}

/*START*/
function __fb_internal_clock_init() {
    console.log("__internal_clock_init");
    console.log("+++++++++++++++++++++++");
    console.log("+++ CLOCK      INIT +++");
    console.log("+++++++++++++++++++++++");
    if (_b_in_clockFlag) {
        console.log("[__internal_clock_init] Warning clock is already running");
        return false;
    }
    _wob_ltime = new Date(); //Generate starting time object
    $(__wdom_output_topbar_dstring).text(fstr_timeDateShort(_wob_ltime));
    console.log("=======================");
    console.log("Date: ", _wob_ltime);
    console.log("Timezone : ", _intTimezone, "\nGMT Adjust: ", _intTimezone);
    console.log("=======================");
    return true;
}
function __fb_internal_clock_Stop() {
    console.log("__internal_clock_Stop");
    if (!_b_in_clockFlag)
        return _b_in_clockFlag; //Lol writing false is effort
    _b_in_clockFlag = false;
    _b_out_clockFlag = false;
    return true;
}
function __fb_internal_clock_Start() {
    console.log("__internal_clock_Start");
    if (_b_in_clockFlag)
        return false;
    _b_in_clockFlag = true;
    _b_out_clockFlag = false;
    __fv_internal_clock();
    __fb_ajaxQuery();
    return true;
}
